function checkObjectValueTypes(obj, types) {
    for (let [name, type] of types) {
        if (typeof obj[name] !== type) {
            throw `option '${name}' should be of type ${type}, not type ${typeof obj[name]}`;
        }
    }
}

function allocateString(str) {
    let utf8Length = Module.lengthBytesUTF8(str),
        ptr = Module._malloc(utf8Length + 1);
    if (ptr === 0) { // i have no idea if this is actually needed with emscripten but let's just assume it is because i get extremely annoyed whenever i see C code that doesn't do it
        throw `out of memory while trying to allocate string ${str}`;
    }

    Module.stringToUTF8(str, ptr, utf8Length + 1);
    return ptr;
}

function withAllocatedString(str, fn) {
    let ptr = allocateString(str);
    try {
        return fn(ptr);
    } finally {
        Module._free(ptr);
    }
}
// we have to duplicate this function because JavaScript is stupid
function* generateWithAllocatedString(str, fn) {
    let ptr = allocateString(str);
    try {
        yield* fn(ptr);
    } finally {
        Module._free(ptr);
    }
}

function indexConversion(str) {
    // @@ for larger strings it would probably make sense to use a data structure that can cope sensibly with strings that are mostly ascii
    let conversion = new Map(),
        utf8BytesIdx = 0,
        utf16UnitsIdx = 0;
    
    for (let character of str) {
        let codepoint = character.codePointAt(0);
        conversion.set(utf8BytesIdx, utf16UnitsIdx);
        
        if (codepoint > 0xFFFF) {
            utf16UnitsIdx += 2;
        } else {
            utf16UnitsIdx += 1;
        }
        if (codepoint < 0x80) {
            utf8BytesIdx += 1;
        } else if (codepoint < 0x0800) {
            utf8BytesIdx += 2;
        } else if (codepoint < 0x10000) {
            utf8BytesIdx += 3;
        } else {
            utf8BytesIdx += 4;
        }
    }
    conversion.set(utf8BytesIdx, utf16UnitsIdx);
    return conversion;
}

export class RE2CompileError {
    constructor (code, description, arg) {
        this.code = code;
        this.description = description;
        this.arg = arg;
    }
}

export class RE2 {
    constructor(source, options={}) {
        let re2Options = {};
        re2Options.utf8 = ('utf8' in options ? options.utf8 : true);
        re2Options.posixSyntax = ('posixSyntax' in options ? options.posixSyntax : false);
        re2Options.longestMatch = ('longestMatch' in options ? options.longestMatch : false);
        re2Options.maxMemory = ('posixSyntax' in options ? options.maxMemory : (2<<80));
        re2Options.literal = ('literal' in options ? options.literal : false);
        re2Options.neverMatchNewline = ('neverMatchNewline' in options ? options.neverMatchNewline : false);
        re2Options.dotMatchesNewline = ('dotMatchesNewline' in options ? options.dotMatchesNewline : false);
        re2Options.neverCapture = ('neverCapture' in options ? options.neverCapture : false);
        re2Options.caseSensitive = ('caseSensitive' in options ? options.caseSensitive : true);

        // only relevant if posixSyntax is true
        re2Options.perlClasses = ('perlClasses' in options ? options.perlClasses : false);
        re2Options.wordBoundaryClasses = ('wordBoundaryClasses' in options ? options.wordBoundaryClasses : false);
        re2Options.oneLine = ('oneLine' in options ? options.oneLine : false);

        checkObjectValueTypes(re2Options, [
            ['utf8', 'boolean'],
            ['posixSyntax', 'boolean'],
            ['longestMatch', 'boolean'],
            ['maxMemory', 'number'],
            ['literal', 'boolean'],
            ['neverMatchNewline', 'boolean'],
            ['dotMatchesNewline', 'boolean'],
            ['neverCapture', 'boolean'],
            ['caseSensitive', 'boolean'],
            ['perlClasses', 'boolean'],
            ['wordBoundaryClasses', 'boolean'],
            ['oneLine', 'boolean'],
        ]);

        this.options = re2Options;
        this._options = Module.RE2_CreateOptions(
            re2Options.utf8,
            re2Options.posixSyntax,
            re2Options.longestMatch,
            false,
            re2Options.maxMemory,
            re2Options.literal,
            re2Options.neverMatchNewline,
            re2Options.dotMatchesNewline,
            re2Options.neverCapture,
            re2Options.caseSensitive,
            re2Options.perlClasses,
            re2Options.wordBoundaryClasses,
            re2Options.oneLine,
        );

        this.source = source;
        this._source = allocateString(source);

        this._regexp = Module.RE2_CreateRegexWithOptions(this._source, this._options);
        if (!Module.RE2_Ok(this._regexp)) {
            let code = Module.RE2_ErrorCode(this._regexp),
                description = Module.RE2_Error(this._regexp),
                arg = Module.RE2_ErrorArg(this._regexp);
            Module._free(this._regexp);
            Module._free(this._source);
            Module._free(this._options);
            throw new RE2CompileError(code, description, arg);
        }

        this.namedGroups = new Map();
        let _namedGroups = Module.RE2_CapturingGroupNames(this._regexp),
            nGroups = Module.RE2_NumberOfCapturingGroups(this._regexp);
        for (let i = 1; i <= nGroups; i++) {
            let name = _namedGroups.get(i);
            if (name !== undefined) {
                this.namedGroups.set(name, i);
            }
        }
        Module._free(_namedGroups);
    }
    
    isFullMatch(text) {
        return withAllocatedString(text, (textptr) => {
            return Module.RE2_TestFullMatch(this._regexp, textptr);
        });
    }
    isPartialMatch(text) {
        return withAllocatedString(text, (textptr) => {
            return Module.RE2_TestPartialMatch(this._regexp, textptr);
        });
    }

    match(text, startPos=0, endPos=null) {
        if (endPos === null) { endPos = Module.lengthBytesUTF8(text); }
        let idxs = indexConversion(text);
        return withAllocatedString(text, (textptr) => {
            let match = Module.RE2_Match(this._regexp, textptr, startPos, endPos),
                size = match.size();
            if (size > 0) {
                let matchGroups = [];
                for (let i = 0; i < size; i++) {
                    let submatch = match.get(i);
                    matchGroups.push([submatch.getStart(), submatch.getEnd()]);
                    Module._free(submatch);
                }

                Module._free(match);
                return new RE2Match(this, text, matchGroups, idxs);
            } else {
                Module._free(match);
                return null;
            }
        });
    }

    // leaks memory if you don't consume all the matches. thanks, JavaScript
    *allMatches(text, startPos=0, endPos=null) {
        if (endPos === null) { endPos = Module.lengthBytesUTF8(text); }
        let that = this, // ES6 was supposed to fix this for us >_<
            idxs = indexConversion(text);
        yield* generateWithAllocatedString(text, function* (textptr) {
            let currentMatch = Module.RE2_Match(that._regexp, textptr, startPos, endPos);
            while (currentMatch.size() > 0) {
                let matchGroups = [],
                    size = currentMatch.size();
                
                for (let i = 0; i < size; i++) {
                    let submatch = currentMatch.get(i);
                    matchGroups.push([submatch.getStart(), submatch.getEnd()]);
                    Module._free(submatch);
                }

                Module._free(currentMatch);

                yield new RE2Match(that, text, matchGroups, idxs);
                
                startPos = matchGroups[0][1];
                currentMatch = Module.RE2_Match(that._regexp, textptr, startPos, endPos);
            }
        });
    }
}

class RE2Match {
    constructor(regexp, text, groups, indices) {
        this.regexp = regexp;
        this.text = text;
        this._groups = groups;
        this._indices = indices;
    }

    group (idx) {
        if ((typeof idx === 'number') && ((idx|0) === idx)) {
            if (idx > (this._groups.length - 1)) {
                throw `index ${idx} is out of range of subpattern matches (maximum is ${this._groups.length - 1})`;
            } else if (this._groups[idx][0] === -1) {
                return null;
            } else {
                return this.text.substring(this._indices.get(this._groups[idx][0]), this._indices.get(this._groups[idx][1]));
            }
        } else if (typeof idx === 'string') {
            if (!this.regexp.namedGroups.has(idx)) {
                throw `there isn't a named submatch called ${idx}`;
            } else {
                return this.group(this.regexp.namedGroups.get(idx));
            }
        } else {
            throw new TypeError('argument to RE2Match.group must be either string or integer');
        }
    }

    groups () {
        let that = this;
        return Array.from((function* () {
            for (let i = 1; i < that._groups.length; i++) {
                yield that.group(i);
            }
        })());
    }

    groupMap() {
        let out = new Map();
        for (let [name, number] of this.regexp.namedGroups) {
            out.set(name, this.group(number));
        }
        return out;
    }

    start () { return this._indices.get(this._groups[0][0]); }
    end () { return this._indices.get(this._groups[0][1]); }
}
