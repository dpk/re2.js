.PHONY: all clean test

all: build/libre2.js build/re2.mjs

clean:
	rm -rf build
	cd re2 && make clean

test: all
	node --experimental-modules test/test-re2.mjs

build:
	mkdir build

re2/obj/libre2.a: .git/modules/re2/HEAD
	cd re2 && emmake $(MAKE)

build/libre2.js: re2/obj/libre2.a re2bindings.cpp | build
	em++ re2bindings.cpp re2/obj/libre2.a -o $@ -std=c++11 -I $(realpath re2) --bind -O3 -s ALLOW_MEMORY_GROWTH=1 --memory-init-file 0

build/re2.mjs: re2.js
	echo 'import Module from "./libre2.js";' > $@
	cat re2.js >> $@
