#include <emscripten/bind.h>
#include <emscripten/val.h>
#include "re2/re2.h"
// #include "util/flags.h"
#include <codecvt>

using std::vector;
using re2::RE2;
using re2::StringPiece;

using namespace emscripten;

// DECLARE_bool(trace_re2);

class RegexMatch {
  public:
    RegexMatch ();
    RegexMatch (int,int);
    int start;
    int end;
    int getStart() { return start; };
    int getEnd() { return end; };
};

RegexMatch::RegexMatch () {}

RegexMatch::RegexMatch (int a, int b) {
  start = a;
  end = b;
}


int RE2_CreateRegex(int regex_pointer) {
  const char* regex = (char*)regex_pointer;

  return (int)(new RE2(regex));
}
int RE2_CreateRegexWithOptions(int regex_pointer, int options_pointer) {
  const char* regex = (char*)regex_pointer;
  RE2::Options* opts = (RE2::Options*)options_pointer;
  
  return (int)(new RE2(regex, *opts));
}
int RE2_CreateOptions(bool utf8,
                      bool posix_syntax,
                      bool longest_match,
                      bool log_errors, // possibly disable?
                      unsigned int max_mem,
                      bool literal,
                      bool never_nl,
                      bool dot_nl,
                      bool never_capture,
                      bool case_sensitive,
                      // posix_syntax only
                      bool perl_classes,
                      bool word_boundary,
                      bool one_line) {
  RE2::Options* opts = new RE2::Options();

  opts->set_utf8(utf8);
  opts->set_posix_syntax(posix_syntax);
  opts->set_longest_match(longest_match);
  opts->set_log_errors(log_errors);
  opts->set_max_mem(max_mem);
  opts->set_literal(literal);
  opts->set_never_nl(never_nl);
  opts->set_dot_nl(dot_nl);
  opts->set_never_capture(never_capture);
  opts->set_case_sensitive(case_sensitive);
  opts->set_perl_classes(perl_classes);
  opts->set_word_boundary(word_boundary);
  opts->set_one_line(one_line);

  return (int)(opts);
}

bool RE2_Ok(int regex_pointer) {
  return ((RE2*)regex_pointer)->ok();
}
std::string RE2_Error(int regex_pointer) {
  return ((RE2*)regex_pointer)->error();
}
int RE2_ErrorCode(int regex_pointer) {
  return (int)((RE2*)regex_pointer)->error_code();
}
std::string RE2_ErrorArg(int regex_pointer) {
  return ((RE2*)regex_pointer)->error_arg();
}

bool RE2_TestFullMatch(int regex_pointer, int text_pointer) {
  const char* match_text = (char*)text_pointer;
  RE2* regex = (RE2*) regex_pointer;
  bool did_match = RE2::FullMatch(match_text, *regex);
  return did_match;
}
bool RE2_TestPartialMatch(int regex_pointer, int text_pointer) {
  const char* match_text = (char*)text_pointer;
  RE2* regex = (RE2*) regex_pointer;
  bool did_match = RE2::PartialMatch(match_text, *regex);
  return did_match;
}

std::vector<RegexMatch> RE2_Match(int regex_pointer, int text_pointer, int startpos, int endpos) {
  // Debug only!
  // re2::FLAGS_trace_re2 = true;

  const char* match_text = (char*)text_pointer;

  int captureGroupCount = ((RE2*)regex_pointer)->NumberOfCapturingGroups();

  std::vector<StringPiece> groups(captureGroupCount + 1);
  std::vector<RegexMatch> result(captureGroupCount + 1);

  bool did_match = ((RE2*)regex_pointer)->Match(match_text, startpos, endpos, RE2::UNANCHORED, &groups[0], groups.size());

  if (did_match) {
    for (size_t i = 0, n = groups.size(); i < n; ++i) {
      const StringPiece& item = groups[i];

      if (item.data() == NULL) {
        RegexMatch rm(-1, -1);
        result[i] = rm;
      } else {
        int match_start = item.begin() - match_text;
        int match_end = match_start + item.size();

        RegexMatch rm(match_start, match_end);
        result[i] = rm;
      }
    }

    return result;
  } else {
    return vector<RegexMatch>();
  }
}

int RE2_NumberOfCapturingGroups(int regex_pointer) {
  RE2* regex = (RE2*) regex_pointer;
  return regex->NumberOfCapturingGroups();
};

std::map<int,std::string> RE2_CapturingGroupNames(int regex_pointer) {
  RE2* regex = (RE2*) regex_pointer;
  return regex->CapturingGroupNames();
}

EMSCRIPTEN_BINDINGS(my_module) {
  register_vector<RegexMatch>("VectorRegexMatch");
  register_map<int,std::string>("MapStringInt");

  function("RE2_CreateRegex", &RE2_CreateRegex);
  function("RE2_CreateRegexWithOptions", &RE2_CreateRegexWithOptions);
  function("RE2_CreateOptions", &RE2_CreateOptions);

  function("RE2_Ok", &RE2_Ok);
  function("RE2_Error", &RE2_Error);
  function("RE2_ErrorCode", &RE2_ErrorCode);
  function("RE2_ErrorArg", &RE2_ErrorArg);

  function("RE2_Match", &RE2_Match);
  function("RE2_TestFullMatch", &RE2_TestFullMatch);
  function("RE2_TestPartialMatch", &RE2_TestPartialMatch);

  function("RE2_NumberOfCapturingGroups", &RE2_NumberOfCapturingGroups);
  function("RE2_CapturingGroupNames", &RE2_CapturingGroupNames);

  class_<RegexMatch>("RegexMatch")
    .constructor<int,int>()
    .function("getStart", &RegexMatch::getStart)
    .function("getEnd", &RegexMatch::getEnd);
}
