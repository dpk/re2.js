import {RE2} from '../build/re2.mjs';
import test from 'tape';

test('full match', function (t) {
    let re = new RE2('abc');
    t.ok(re.isFullMatch('abc'));
    t.end();
});

test('partial match', function (t) {
    let re = new RE2('abc');
    t.ok(re.isPartialMatch('xyz abc def'));
    t.end();
});

test('find all matches with submatch extraction', function (t) {
    t.plan(7);
    let re = new RE2('(ab)(c|d)'),
        matches = Array.from(re.allMatches('abc abd'));
    
    t.equal(matches.length, 2, 'there are two matches');
    let firstMatch = matches[0];
    t.equal(firstMatch.group(0), 'abc', 'whole of first match');
    t.equal(firstMatch.group(1), 'ab', 'first submatch of first match');
    t.equal(firstMatch.group(2), 'c', 'second submatch of first match');
    let secondMatch = matches[1];
    t.equal(secondMatch.group(0), 'abd', 'whole of second match');
    t.equal(secondMatch.group(1), 'ab', 'first submatch of second match');
    t.equal(secondMatch.group(2), 'd', 'second submatch of second match');
});

test('correct match extraction with unicode characters', function (t) {
    let re = new RE2('π'),
        match = re.match('Ψάπφω');

    t.equal(match.group(0), 'π', 'should be able to extract pi from Psappho');
    t.end();
});

test('extraction of non-participating submatches returns null', function (t) {
    let re = new RE2('(abc)|(def)'),
        match = re.match('abcxyz');

    t.equal(match.group(1), 'abc', 'group 1 is abc');
    t.equal(match.group(2), null, 'group 2 is null');
    t.end();
});
